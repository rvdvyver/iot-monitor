package com.rvdvyver.iot.iotendpoint;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestOperations;

@Configuration
public class ApplicationConfiguration {

    @Value("${ns.api.username}")
    private String username;

    @Value("${ns.api.password}")
    private String password;

    @Bean
    public RestOperations restOperations(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.basicAuthorization(username, password).build();
    }

}
