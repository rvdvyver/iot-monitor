package com.rvdvyver.iot.iotendpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses = ApplicationConfiguration.class)
public class IotEndPointApplication {

	public static void main(String[] args) {
		SpringApplication.run(IotEndPointApplication.class, args);
	}
}
