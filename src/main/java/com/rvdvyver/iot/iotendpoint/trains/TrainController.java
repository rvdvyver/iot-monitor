package com.rvdvyver.iot.iotendpoint.trains;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Controller
public class TrainController {

    @Autowired
    private NsApiService nsApiService;

    @GetMapping(value = "/api/home/nexttrain", produces = "text/plain")
    @ResponseBody
    public String nextTrainAsText() {
        return nsApiService.getNextTrainTimeForMaarnUtrecht().toString();
    }
}
