package com.rvdvyver.iot.iotendpoint.trains;

import org.joda.time.LocalDateTime;
import org.springframework.data.convert.JodaTimeConverters;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public class Train {

    @XmlElement(name = "RitNummer")
    private String routeNumber;

    @XmlElement(name = "VertrekTijd")
    @XmlJavaTypeAdapter(LocalDateTimeConverter.class)
    private LocalDateTime departureTime;

    @XmlElement(name = "EindBestemming")
    private String endStation;

    @XmlElement(name = "TreinSoort")
    private String trainType;

    @XmlElement(name = "VertrekSpoor")
    private String platform;
    
    public String getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public String getEndStation() {
        return endStation;
    }

    public void setEndStation(String endStation) {
        this.endStation = endStation;
    }

    public String getTrainType() {
        return trainType;
    }

    public void setTrainType(String trainType) {
        this.trainType = trainType;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
}
