package com.rvdvyver.iot.iotendpoint.trains;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "ActueleVertrekTijden")
@XmlAccessorType(XmlAccessType.FIELD)
public class Departures {

    @XmlElement(name = "VertrekkendeTrein")
    private List<Train> trains = new ArrayList<>();

    /**
     * Getter for property 'trains'.
     *
     * @return Value for property 'trains'.
     */
    public List<Train> getTrains() {
        return trains;
    }

    /**
     * Setter for property 'trains'.
     *
     * @param trains Value to set for property 'trains'.
     */
    public void setTrains(List<Train> trains) {
        this.trains = trains;
    }
}
