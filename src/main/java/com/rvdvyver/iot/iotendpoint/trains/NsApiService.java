package com.rvdvyver.iot.iotendpoint.trains;

import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import java.util.Comparator;
import java.util.Optional;

@Service
public class NsApiService {

    public static final String EUROPE_PARIS = "Europe/Paris";
    public static final String TRAIN_MAARN_UTRECHT = "https://webservices.ns.nl/ns-api-avt?station=maarn";

    @Autowired
    private RestOperations restOperations;

    public Long getNextTrainTimeForMaarnUtrecht() {
        String url = TRAIN_MAARN_UTRECHT;
        ResponseEntity<Departures> response = restOperations.getForEntity(url, Departures.class);

        Departures departures = response.getBody();
        Optional<Train> first = departures.getTrains().stream().filter(train -> train.getPlatform().equals("1")).sorted(Comparator.comparing(Train::getDepartureTime)).findFirst();
        if (first.isPresent()) {
            LocalDateTime nextTimeDeparture = first.get().getDepartureTime();
            LocalDateTime now = new LocalDateTime(DateTimeZone.forID(EUROPE_PARIS));
            Duration duration = new Duration(now.toDateTime(), nextTimeDeparture.toDateTime());
            return duration.getStandardMinutes();
        } else {
            return Long.MAX_VALUE;
        }
    }
}
