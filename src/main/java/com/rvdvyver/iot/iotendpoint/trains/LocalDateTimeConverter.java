package com.rvdvyver.iot.iotendpoint.trains;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class LocalDateTimeConverter extends XmlAdapter<String, LocalDateTime> {

    private static DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    @Override
    public LocalDateTime unmarshal(String vt) throws Exception {
        return dateTimeFormatter.parseLocalDateTime(vt);
    }

    @Override
    public String marshal(LocalDateTime bt) throws Exception {
        return dateTimeFormatter.print(bt);
    }

}
